from pygame import *
from random import *

class Sounds(object):  #background music...designed to work for multiple songs...but yeah
    def __init__(self):
        mixer.pre_init(44100,-16,2, 1024)
        mixer.init()
        self.sprees=[mixer.Sound("sounds/spree_3.wav"), 
        mixer.Sound("sounds/spree_4.wav"),
        mixer.Sound("sounds/spree_5.wav"),
        mixer.Sound("sounds/spree_6.wav"),
        mixer.Sound("sounds/spree_7.wav"),
        mixer.Sound("sounds/spree_8.wav"),
        mixer.Sound("sounds/spree_9.wav"),
        mixer.Sound("sounds/spree_10.wav")]

        self.background=[mixer.Sound("sounds/Bastion04.wav"), mixer.Sound("sounds/Bastion13.wav")]
        shuffle(self.background)

        self.explosion = mixer.Sound("sounds/explosion.wav")
        self.lazer = mixer.Sound("sounds/laserbeam.wav")
        self.missle = mixer.Sound("sounds/missle.wav")
        self.shot = mixer.Sound("sounds/shot.wav")
        
    def play_spree(self, spree):

        if spree >= 3:
            if spree > 10:
                spree = 10

            self.sprees[spree-3].play()

    def play_background(self):
        self.currentbackground=self.background[0]
        self.background=self.background[1:]+[self.background[0]]
        self.currentbackground.set_volume(0.5)
        self.currentbackground.play()

    def play_explosion(self):
        self.explosion.play()

    def play_lazer(self):
        self.lazer.play()

    def play_missle(self):
        self.missle.play()

    def play_shot(self):
        self.shot.play()
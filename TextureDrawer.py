from OpenGL.GL import *

import numpy

class TextureDrawer:
    POS_ATTRIB = "aPos"
    TEXPOS_ATTRIB = "aTexCoord"
    TEXSAMPLER_UNIFORM = "uTexture"

    def __init__(self):
        self.vertex_buffer = glGenBuffers(1)

        vertices = [
                 1.0,  1.0,
                -1.0,  1.0,
                -1.0, -1.0,
                -1.0, -1.0,
                 1.0, -1.0,
                 1.0,  1.0,
                 # tex coords
                 1.0,  1.0,
                 0.0,  1.0,
                 0.0,  0.0,
                 0.0,  0.0,
                 1.0,  0.0,
                 1.0,  1.0
                ]

        vertices = numpy.array(vertices, dtype=numpy.float32)


        glBindBuffer(GL_ARRAY_BUFFER, self.vertex_buffer)

        # Send the data over to the buffer
        glBufferData(GL_ARRAY_BUFFER, 24*4, vertices, GL_STATIC_DRAW)

        glBindBuffer(GL_ARRAY_BUFFER, 0)

    def draw(self, texture, shader):
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, texture.id)
        shader.setUniform1i(TextureDrawer.TEXSAMPLER_UNIFORM, 0)

        pos_location = shader.getAttribLocation(TextureDrawer.POS_ATTRIB)
        texcoord_location = shader.getAttribLocation(TextureDrawer.TEXPOS_ATTRIB)

        glEnableVertexAttribArray(pos_location)
        glEnableVertexAttribArray(texcoord_location)

        glBindBuffer(GL_ARRAY_BUFFER, self.vertex_buffer)

        # Describe the position data layout in the buffer
        glVertexAttribPointer(pos_location, 2, GL_FLOAT, False, 0, ctypes.c_void_p(0))
        glVertexAttribPointer(texcoord_location, 2, GL_FLOAT, False, 0, ctypes.c_void_p(12*4))

        glDrawArrays(GL_TRIANGLES, 0, 6)

        glDisableVertexAttribArray(pos_location)
        glDisableVertexAttribArray(texcoord_location)

        glBindBuffer(GL_ARRAY_BUFFER, 0)

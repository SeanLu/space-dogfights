from OpenGL.GL import *

import sys

class Texture:
    def __init__(self, imagedata, size):
        self.id = glGenTextures(1)
        self.size = size
        glBindTexture(GL_TEXTURE_2D, self.id)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size[0], size[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, imagedata)

        #def chunks(l, n):
        #    """ Yield successive n-sized chunks from l.
        #    """
        #    for i in xrange(0, len(l), n):
        #        yield l[i:i+n]

        #for lst in chunks(imagedata, 4):
        #    print ':'.join(x.encode('hex') for x in lst)
        #sys.exit()

    def bind(self):
        glBindTexture(GL_TEXTURE_2D, self.id)

    def delete(self):
        glDeleteTextures(self.id)

from Base import *
from pygame import *
from math import *
from random import *

class Asteroid(Base):
    def __init__(self, size, x=None, y=None, vx=None, vy=None):
        self.size = size
        self.r = size * 5

        if not x or not y or not vx or not vy:
            angle = random()*2*pi

            critical_angle = atan(SCREEN_SIZE[1]/SCREEN_SIZE[0])
            if 0 <= angle < critical_angle:
                x_offset = SCREEN_SIZE[0]/2 + 2*self.r
                y_offset = x_offset * tan(angle)
            elif angle < pi-critical_angle:
                y_offset = SCREEN_SIZE[1]/2 + 2*self.r
                if abs(angle-pi/2)<0.001:
                    x_offset = 0
                else:
                    x_offset = y_offset / tan(angle)
            elif angle < pi+critical_angle:
                x_offset = -(SCREEN_SIZE[0]/2 + 2*self.r)
                y_offset = x_offset * tan(angle)
            elif angle < 2*pi-critical_angle:
                y_offset = -(SCREEN_SIZE[1]/2 + 2*self.r)
                if abs(angle-3*pi/2)<0.001:
                    x_offset = 0
                else:
                    x_offset = y_offset / tan(angle)
            else:
                x_offset = SCREEN_SIZE[0]/2 + self.r
                y_offset = x_offset * tan(angle)

            trajectory = (angle - pi*3/2) + random()*pi/2
            super(Asteroid,self).__init__(SCREEN_SIZE[0]/2 + x_offset, 
                SCREEN_SIZE[1]/2 + y_offset, 
                cos(trajectory)*randint(1,3),
                sin(trajectory)*randint(1,3),
                self.r)
        else:
            super(Asteroid,self).__init__(x,y,vx,vy,self.r)

        self.health = ceil(float(size**2)*2/3)
        self.power = self.size**2
        self.image = transform.scale(ASTEROID_IMAGE, (self.r * 2, self.r * 2))
    
    def update(self):
        self.prevx=self.x
        self.prevy=self.y

        self.x += self.vx
        self.y += self.vy

        if self.x+self.r < 0 or self.x-self.r > SCREEN_SIZE[0] or self.y+self.r < 0 or self.y-self.r > SCREEN_SIZE[1]:
            # Check if it is going towards the middle of the screen, if yes, this is an asteroid that has recently been generated and should not be removed
            if self.vx*(SCREEN_SIZE[0]-self.x) + self.vy*(SCREEN_SIZE[1]-self.y) < 0:
                self.exists = False

    def draw(self):
        #this line is for testing purposes
        #draw.circle(screen, (255, 0, 0), (int(self.x), int(self.y)), self.r)
        screen.blit(self.image, (int(self.x - self.r), int(self.y - self.r)))

    def die(self, breakup=True, asteroid_list=[]):
        if breakup and ASTEROID_BREAKUP[self.size]:
            # Random breakup pattern
            new_asteroids_map = choice(ASTEROID_BREAKUP[self.size])
            new_asteroids_sizes = []
            largest_size = 0
            for size, amount in new_asteroids_map.iteritems():
                for i in range(amount):
                    new_asteroids_sizes.append(size)
                if size > largest_size:
                    largest_size = size
            shuffle(new_asteroids_sizes)
            total_number = len(new_asteroids_sizes)
            average_size = float(sum(new_asteroids_sizes))/total_number
            average_speed = randint(1,5)
            start_angle = random()*2*pi
            angle_delta = 2*pi/total_number
            angle = start_angle
            for size in new_asteroids_sizes:
                speed = average_size**2/size**2 * average_speed
                asteroid_list.append(Asteroid(size, self.x+8*largest_size/sin(angle_delta/2)*cos(angle), self.y+8*largest_size/sin(angle_delta/2)*sin(angle), self.vx+speed*cos(angle), self.vy+speed*sin(angle)))
                angle += angle_delta

        super(Asteroid,self).die()

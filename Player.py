from Base import *
from Bullet import *
from pygame import *
from math import *
from Powerup import *
from Explosion import Explosion
from Sound import *

import math

class Player(Base):
    images=[]
    ACTION_NORMAL = 0
    ACTION_SHOOTING = 1
    ACTION_DYING = 2
    NUM_CACHED = 180
    MAX_TRAIL_SEGMENTS = 40
    NUM_DYING_FRAMES = 20
    NUM_INVULN_FRAMES = 50

    def __init__(self, player_number):
        if player_number == 1:
            super(Player,self).__init__(100,100,2,2,PLAYER_RADIUS,5)
            self.scoreboard_pos = (10, 10)
        if player_number == 2:
            super(Player,self).__init__(SCREEN_SIZE[0] - 100, SCREEN_SIZE[1] - 100,-2,-2,PLAYER_RADIUS,5)
            self.scoreboard_pos = (SCREEN_SIZE[0] - 150, SCREEN_SIZE[1] - 95)
        if player_number == 3:
            super(Player,self).__init__(100, SCREEN_SIZE[1] - 100, 2,-2,PLAYER_RADIUS,5)
            self.scoreboard_pos = (10, SCREEN_SIZE[1] - 95)
        if player_number == 4:
            super(Player,self).__init__(SCREEN_SIZE[0] - 100, 100, -2,2,PLAYER_RADIUS,5)
            self.scoreboard_pos = (SCREEN_SIZE[0] - 150, 10)

        self.player_number = player_number
        self.speed_limit = 10
        self.max_accel = 0.2
        self.death_count = 0
        self.kill_count = 0
        self.kill_spree = 0
        self.invulnerability = Player.NUM_INVULN_FRAMES
        
        self.action = Player.ACTION_NORMAL
        self.animation_counter = 0
        self.max_health = 5
        self.regen_rate = 250
        self.regen_counter = self.regen_rate

        # for shooting
        self.gun_type = GUN_TYPE["DEFAULT"]
        self.fire_rate = FIRE_RATE[self.gun_type]
        self.fire_cd = 0

        self.past_positions=[(self.x, self.y)]

        self.powerup = -1
        self.powerup_level = 0

        self.shield_radius = int(self.r*2)
        self.shield_screen = Surface((self.shield_radius*2, self.shield_radius*2))

        self.shooting_lazer = False

        # image
        if self.player_number == 1:
            self.image = ROUNDSHIP_IMAGE
        elif self.player_number == 2:
            self.image = TRIANGLESHIP_IMAGE
        elif self.player_number == 3:
            self.image = CIRCLESHIP_IMAGE
        elif self.player_number == 4:
            self.image = TRIDENTSHIP_IMAGE
        else:
            self.image = None

        if self.image is not None:
            self.rotated_image_cache = []
            for i in range(Player.NUM_CACHED):
                old_center = self.image.get_rect().center
                rotated = transform.rotate(self.image, 360/Player.NUM_CACHED * i)
                new_rect = rotated.get_rect().copy()
                new_rect.center = old_center
                self.rotated_image_cache.append((new_rect, rotated))

        self.respawn()

    def drawPlayerImage(self, alpha=255):
        # angle will be used as counterclockwise
        angle = self.get_angle_cc()

        image = None
        if self.image != None:
            while angle < 0:
                angle += 360
            while angle > 360:
                angle -= 360
            rect, image = self.rotated_image_cache[int(angle/(360/Player.NUM_CACHED)) % len(self.rotated_image_cache)]
            new_rect = rect.copy()
            new_rect.x += self.x - self.r
            new_rect.y += self.y - self.r
            tempImage = image.copy()
            tempImage.fill((255, 255, 255, alpha), None, BLEND_RGBA_MULT)
            screen.blit(tempImage, new_rect)

    def drawGlow(self):
        if self.action == Player.ACTION_SHOOTING and self.powerup == Powerup.TYPE_LAZER:
            self.draw_laser()

        if self.action == Player.ACTION_DYING and self.image is not None:
            self.drawPlayerImage(int((1 - float(self.animation_counter) / Player.NUM_DYING_FRAMES) * 255))

        if self.invulnerability > 0:
            interp = 0.5 * math.sin((1 - float(self.invulnerability-1) / Player.NUM_INVULN_FRAMES) * math.pi * 10 - math.pi/2) + 0.5
            self.drawPlayerImage(int(interp * 255))

        self.drawShield(1, 0)
        self.drawShield()

    def drawPlayerTrail(self, line_batch):
        for i in range(len(self.past_positions)-1):
            alpha1 = 1 - float(i)/Player.MAX_TRAIL_SEGMENTS
            alpha2 = 1 - float(i+1)/Player.MAX_TRAIL_SEGMENTS
            color1 = max(1 - 1.5*float(i)/Player.MAX_TRAIL_SEGMENTS - 0.25, 0)
            color2 = max(1 - 1.5*float(i+1)/Player.MAX_TRAIL_SEGMENTS - 0.25, 0)
            line_batch.line(self.past_positions[i], self.past_positions[i+1], (1.0, color1, 0.0, alpha1), (1.0, color2, 0.0, alpha2))

    def drawShield(self, alpha=10, radius=2):
        if self.health > 5:
            self.shield_screen.fill((0, 0, 0))
            self.shield_screen.set_colorkey((0, 0, 0))
            draw.circle(self.shield_screen, (255, 255, 255), (self.shield_radius, self.shield_radius), self.shield_radius, radius)
            self.shield_screen.set_alpha(min(int(alpha*self.health/2), 255))
            screen.blit(self.shield_screen, (int(self.x - self.shield_radius), int(self.y - self.shield_radius)))

    def draw(self):
        health = int(ceil(self.health))
        # draw lasers
        if self.action == Player.ACTION_SHOOTING and self.powerup == Powerup.TYPE_LAZER:
            self.draw_laser()
        self.drawPlayerImage()
        """
        elif self.action == Player.ACTION_NORMAL or self.action == Player.ACTION_SHOOTING:
            if self.invulnerability > 0:
                if self.player_number == 1:
                    draw.rect(screen, (0,150,0), (self.x - self.r, self.y -self.r, self.r*2, self.r*2), health)
                elif self.player_number == 2:
                    draw.rect(screen, (0,0,150), (self.x - self.r, self.y -self.r, self.r*2, self.r*2), health)
                elif self.player_number == 3:
                    draw.rect(screen, (150,150,0), (self.x - self.r, self.y -self.r, self.r*2, self.r*2), health)
                elif self.player_number == 4:
                    draw.rect(screen, (0,150,150), (self.x - self.r, self.y -self.r, self.r*2, self.r*2), health)
            else:
                if self.player_number == 1:
                    draw.rect(screen, (0,255,0), (self.x - self.r, self.y -self.r, self.r*2, self.r*2), health)
                elif self.player_number == 2:
                    draw.rect(screen, (0,0,255), (self.x - self.r, self.y -self.r, self.r*2, self.r*2), health)
                elif self.player_number == 3:
                    draw.rect(screen, (255,255,0), (self.x - self.r, self.y -self.r, self.r*2, self.r*2), health)
                elif self.player_number == 4:
                    draw.rect(screen, (0,255,255), (self.x - self.r, self.y -self.r, self.r*2, self.r*2), health)
            draw.line(screen, (255,0,0), (self.x, self.y), (self.x + self.vx, self.y+self.vy))
        elif self.action == Player.ACTION_DYING:
            draw.rect(screen, (255,0,0), (self.x - self.r, self.y -self.r, self.r*2, self.r*2))
        """
        
        self.drawShield()

    def draw_laser(self):
        speed = sqrt(self.vx**2 + self.vy**2)
        vx = self.vx * sqrt(2) / speed
        vy = self.vy * sqrt(2) / speed
        draw.line(screen, (255,0,0), (self.x, self.y), (self.x + vx * SCREEN_SIZE[0], self.y + vy * SCREEN_SIZE[1]), self.powerup_level)

    def update(self, keys=None, bullets=None, explosions=None):
        # sanity check
        if keys is None:
            print "ERROR: Player.update() is called with keys=None"
            quit()
        if bullets is None:
            print "ERROR: Player.update() is called with bullets=None"
            quit()

        if self.invulnerability > 0:
            self.invulnerability -= 1

        ###check movement###

        if self.action == Player.ACTION_NORMAL or self.action == Player.ACTION_SHOOTING:
            accel = self.max_accel

            key_accel = 0.2
            accelx = 0
            accely = 0
            self.prevx = self.x
            self.prevy = self.y

            if self.player_number == 1:
                if keys[K_w]:
                    accely -= key_accel
                if keys[K_a]:
                    accelx -= key_accel
                if keys[K_s]:
                    accely += key_accel
                if keys[K_d]:
                    accelx += key_accel
            elif self.player_number ==2:
                if keys[K_UP]:
                    accely -= key_accel
                if keys[K_LEFT]:
                    accelx -= key_accel
                if keys[K_DOWN]:
                    accely += key_accel
                if keys[K_RIGHT]:
                    accelx += key_accel
            elif self.player_number ==3:
                if keys[K_7] or keys[K_8]:
                    accely -= key_accel
                if keys[K_y]:
                    accelx -= key_accel
                if keys[K_u]:
                    accely += key_accel
                if keys[K_i]:
                    accelx += key_accel
            elif self.player_number ==4:
                if keys[K_0]:
                    accely -= key_accel
                if keys[K_1]:
                    accelx -= key_accel
                if keys[K_2]:
                    accely += key_accel
                if keys[K_3]:
                    accelx += key_accel

            raw_accel = sqrt(accelx**2 + accely**2)
            if raw_accel > accel:
                accelx = accelx * accel / raw_accel
                accely = accely * accel / raw_accel
            
            if self.powerup == Powerup.TYPE_SPEED:
                if accelx*self.vx < 0:
                    self.vx = 0
                if accely*self.vy < 0:
                    self.vy = 0

            newvx = self.vx + accelx
            newvy = self.vy + accely

            new_speed = sqrt(newvx**2 + newvy**2)
            if new_speed < self.speed_limit:
                self.vx = newvx
                self.vy = newvy
            else:
                self.vx = newvx * self.speed_limit / new_speed
                self.vy = newvy * self.speed_limit / new_speed

            self.x+=self.vx
            self.y+=self.vy
        
            # shooting:
            if self.player_number == 1 and keys[K_SPACE]:
                self.fire(bullets)
            elif self.player_number == 2 and keys[K_RETURN]:
                self.fire(bullets)
            elif self.player_number == 3 and (keys[K_g] or keys[K_b]):
                self.fire(bullets)
            elif self.player_number == 4 and keys[K_4]:
                self.fire(bullets)
            elif self.action != Player.ACTION_DYING:
                self.action = Player.ACTION_NORMAL

            if self.powerup == Powerup.TYPE_SHIELD:
                if self.x < 0 or self.x > SCREEN_SIZE[0]:
                    if self.x < 0:
                        self.x = 0
                    else:
                        self.x = SCREEN_SIZE[0]
                    self.vx *= -1
                if self.y < 0 or self.y > SCREEN_SIZE[1]:
                    if self.y < 0:
                        self.y = 0
                    else:
                        self.y = SCREEN_SIZE[1]
                    self.vy *= -1
            else:
                if self.x < 0 or self.x > SCREEN_SIZE[0] or self.y < 0 or self.y > SCREEN_SIZE[1]:
                    self.x -= self.vx
                    self.y -= self.vy
                    self.die()

        ###regen###
        if self.action == Player.ACTION_NORMAL or self.action == Player.ACTION_SHOOTING:
            self.regen_counter -= 1
            if self.health >= self.max_health:
                self.regen_counter = self.regen_rate
            elif self.regen_counter <= 0:
                self.health += 1
                self.regen_counter = self.regen_rate

        ##switch animation##
        self.animation_counter -= 1
        if self.animation_counter == 0:
            if self.action == Player.ACTION_NORMAL:
                pass
            if self.action == Player.ACTION_SHOOTING:
                self.action = Player.ACTION_NORMAL
            if self.action == Player.ACTION_DYING:
                explosions.append(Explosion((self.x, self.y), self.shield_radius))
                sounds.play_explosion()
                self.respawn()
        
        ##fire cd##:
        self.fire_cd -= 1
        if self.action != Player.ACTION_SHOOTING:
            self.shooting_lazer = False

        self.past_positions.insert(0, (self.x, self.y))
        if len(self.past_positions) > Player.MAX_TRAIL_SEGMENTS:
            self.past_positions = self.past_positions[:Player.MAX_TRAIL_SEGMENTS]

    def respawn(self):
        if self.player_number == 1:
            self.x = 100
            self.y = 100
            self.vx = 2
            self.vy = 2
        elif self.player_number == 2:
            self.x = SCREEN_SIZE[0] - 100
            self.y = SCREEN_SIZE[1] - 100
            self.vx = -2
            self.vy = -2
        elif self.player_number == 3:
            self.x = 100
            self.y = SCREEN_SIZE[1] - 100
            self.vx = 2
            self.vy = -2
        elif self.player_number == 4:
            self.x = SCREEN_SIZE[0] - 100
            self.y = 100
            self.vx = -2
            self.vy = 2
        self.prevx = self.x
        self.prevy = self.y
        self.invulnerability = Player.NUM_INVULN_FRAMES
        self.health = 5
        self.regen_counter = self.regen_rate
        self.action = Player.ACTION_NORMAL
        self.past_positions=[]

        if self.powerup not in range(0,4):
            self.powerup_level = 0
        if self.powerup_level <= 0:
            self.powerup_level = 1
            self.powerup = randint(0,4)
        self.update_powerup()

    def die(self):
        if self.action != Player.ACTION_DYING:
            self.action = Player.ACTION_DYING
            self.death_count += 1
            self.kill_spree = 0
            self.powerup_level -= 2
            self.animation_counter = Player.NUM_DYING_FRAMES
                
    def fire(self, bullets):
        if self.fire_cd > 0:
            return

        self.action = Player.ACTION_SHOOTING
        if self.powerup != Powerup.TYPE_LAZER:
            bullet_amount = 1
            if self.powerup==Powerup.TYPE_SPLITSHOT:
                bullet_amount = 1 + self.powerup_level*2
                if bullet_amount < 1:
                    bullet_amount = 1
        
            angle_diff = pi/3 / (bullet_amount + 1)
            angle = atan2(self.vy, self.vx) - pi/6

            if self.powerup != Powerup.TYPE_HOMINGMISSILE:
                sounds.play_shot()
                
            for i in range(bullet_amount):
                angle += angle_diff
                if self.powerup == Powerup.TYPE_HOMINGMISSILE:
                    sounds.play_missle()
                    bullets.append(HomingMissile(self.x, self.y, cos(angle), sin(angle), self, self.gun_type))
                else:
                    bullets.append(Bullet(self.x, self.y, cos(angle), sin(angle), self, self.gun_type))
            self.fire_cd = self.fire_rate
        else:
            if not self.shooting_lazer:
                sounds.play_lazer()
            self.shooting_lazer = True
            

    def update_powerup(self):
        self.speed_limit = 10
        self.max_accel = 0.2
        self.gun_type = GUN_TYPE["DEFAULT"]
        self.fire_rate = FIRE_RATE[self.gun_type]
        self.max_health = 5
        self.regen_rate = 250
        self.health = self.max_health

        if self.powerup == Powerup.TYPE_SPEED:
            self.speed_limit = 10 + 5*self.powerup_level
            self.max_accel = 0.2 + 0.1*self.powerup_level
            self.fire_rate = int(self.fire_rate / (float(self.powerup_level)/2 + 1))
        elif self.powerup == Powerup.TYPE_SHIELD:
            self.max_health += self.powerup_level * 5
            self.regen_rate = int(self.regen_rate / (float(self.powerup_level)/2 + 1))
            self.health = self.max_health
        elif self.powerup == Powerup.TYPE_LAZER:
            self.gun_type = GUN_TYPE["LASER"]
            self.fire_cd = 0
        elif self.powerup == Powerup.TYPE_HOMINGMISSILE:
            self.gun_type = GUN_TYPE["HOMINGMISSILE"]
            self.fire_rate = 5 + 10/self.powerup_level


        self.power = self.health + 1

    def name(self):
        return PLAYER_NAMES[self.player_number]


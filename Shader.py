from OpenGL.GL import *

class Shader:
    def _shader_type_to_string(self, type):
        if type == GL_VERTEX_SHADER:
            return "vertex shader"
        elif type == GL_FRAGMENT_SHADER:
            return "fragment shader"
        else:
            return "unknown shader type"

    def _make_shader_from_text(self, type, text):
        shader_id = glCreateShader(type)
        glShaderSource(shader_id, text)
        glCompileShader(shader_id)
        if glGetShaderiv(shader_id, GL_COMPILE_STATUS) != GL_TRUE:
            raise RuntimeError("Error compiling " + self._shader_type_to_string(type) + "!\n" + glGetShaderInfoLog(shader_id))
        return shader_id

    def __init__(self, vert_shader_text, frag_shader_text):
        vertex_shader = self._make_shader_from_text(GL_VERTEX_SHADER, vert_shader_text)
        fragment_shader = self._make_shader_from_text(GL_FRAGMENT_SHADER, frag_shader_text)

        program = glCreateProgram()
        glAttachShader(program, vertex_shader)
        glAttachShader(program, fragment_shader)

        glLinkProgram(program)
        if glGetProgramiv(program, GL_LINK_STATUS) != GL_TRUE:
            raise RuntimeError("Error linking shader!\n" + glGetProgramInfoLog(program))

        self.id = program
        self.uniform_cache = {}
        self.attribute_cache = {}

    def bind(self):
        glUseProgram(self.id)

    def getUniformLocation(self, uniform_name):
        location = 0
        if uniform_name in self.uniform_cache:
            location = self.uniform_cache[uniform_name]
        else:
            location = glGetUniformLocation(self.id, uniform_name)
            self.uniform_cache[uniform_name] = location
        return location

    def getAttribLocation(self, attribute_name):
        location = 0
        if attribute_name in self.attribute_cache:
            location = self.attribute_cache[attribute_name]
        else:
            location = glGetAttribLocation(self.id, attribute_name)
            self.attribute_cache[attribute_name] = location
        return location

    def setUniform1f(self, uniform_name, a):
        uniform_location = self.getUniformLocation(uniform_name)
        glUniform1f(uniform_location, a)

    def setUniform1i(self, uniform_name, a):
        uniform_location = self.getUniformLocation(uniform_name)
        glUniform1i(uniform_location, a)

    def setUniform2f(self, uniform_name, a, b):
        uniform_location = self.getUniformLocation(uniform_name)
        glUniform2f(uniform_location, a, b)

from OpenGL.GL import *

from Base import *
from pygame import *

import math
import numpy

from TextureDrawer import TextureDrawer
from LineDrawer import LineDrawer

class Explosion:
    POS_ATTRIB = TextureDrawer.POS_ATTRIB
    COLOR_ATTRIB = LineDrawer.COLOR_ATTRIB

    EDGE_WIDTH=1.2
    NUM_SEGMENTS=100
    MAX_LIFE = 50
    MAX_RADIUS = 100

    VERTEX_BUFFER_RAW = None
    VERTEX_BUFFER = None

    def __init__(self, pos, radius):
        self.color = (1.0, 1.0, 1.0, 1.0)
        self.position = pos
        self.life = Explosion.MAX_LIFE
        self.circle_radius = radius
        self.circle_screen = Surface((2*radius, 2*radius))

        if (Explosion.VERTEX_BUFFER == None):
            Explosion.VERTEX_BUFFER = glGenBuffers(1)

            Explosion.VERTEX_BUFFER_RAW = []

            for i in range(Explosion.NUM_SEGMENTS):
                j = (i+1) % Explosion.NUM_SEGMENTS
                angle1 = math.pi * 2 * (float(i)/Explosion.NUM_SEGMENTS)
                angle2 = math.pi * 2 * (float(j)/Explosion.NUM_SEGMENTS)
                x1 = math.cos(angle1)
                y1 = math.sin(angle1)
                x2 = math.cos(angle2)
                y2 = math.sin(angle2)
                x1_far = Explosion.EDGE_WIDTH * math.cos(angle1)
                y1_far = Explosion.EDGE_WIDTH * math.sin(angle1)
                x2_far = Explosion.EDGE_WIDTH * math.cos(angle2)
                y2_far = Explosion.EDGE_WIDTH * math.sin(angle2)
                Explosion.VERTEX_BUFFER_RAW.extend([
                    x1, y1, 0.0,
                    0.0,  0.0, 0.0,
                    x2, y2, 0.0
                ])
                Explosion.VERTEX_BUFFER_RAW.extend([
                    x1, y1, 0.0,
                    x2, y2, 0.0,
                    x2_far, y2_far, 1.0
                ])
                Explosion.VERTEX_BUFFER_RAW.extend([
                    x2_far, y2_far, 1.0,
                    x1_far, y1_far, 1.0,
                    x1, y1, 0.0
                ])

            vertices = numpy.array(Explosion.VERTEX_BUFFER_RAW, dtype=numpy.float32)

            glBindBuffer(GL_ARRAY_BUFFER, Explosion.VERTEX_BUFFER)
            glBufferData(GL_ARRAY_BUFFER, len(Explosion.VERTEX_BUFFER_RAW)*4, vertices, GL_STATIC_DRAW)
            glBindBuffer(GL_ARRAY_BUFFER, 0)

    def update(self):
        self.life -= 1

    def drawGlow(self):
        self.circle_screen.fill((0, 0, 0))
        self.circle_screen.set_colorkey((0, 0, 0))
        life_percent = self.get_life_percent()
        draw.circle(self.circle_screen, (255, 255, 255), (self.circle_radius, self.circle_radius), int(self.circle_radius * (0.25 + 0.75 * math.sin((1-life_percent) * math.pi))))
        self.circle_screen.set_alpha(min(int(255*life_percent), 255))
        screen.blit(self.circle_screen, (int(self.position[0] - self.circle_radius), int(self.position[1] - self.circle_radius)))

    def get_life_percent(self):
        return math.pow(1000, -(1-(float(self.life)/Explosion.MAX_LIFE)))

    def draw(self, shader):
        glPushMatrix()
        glDisable(GL_CULL_FACE)

        glTranslate(self.position[0], self.position[1], 0)
        life_percent = self.get_life_percent()
        scale = Explosion.MAX_RADIUS*(1 - life_percent)
        glScale(scale, scale, 1)

        shader.setUniform1f("uAlpha", life_percent/4)

        pos_location = shader.getAttribLocation(Explosion.POS_ATTRIB)
        color_location = shader.getAttribLocation(Explosion.COLOR_ATTRIB)

        glEnableVertexAttribArray(pos_location)
        glEnableVertexAttribArray(color_location)

        glBindBuffer(GL_ARRAY_BUFFER, Explosion.VERTEX_BUFFER)

        # Describe the position data layout in the buffer
        glVertexAttribPointer(pos_location, 2, GL_FLOAT, False, 3*4, ctypes.c_void_p(0))
        glVertexAttribPointer(color_location, 1, GL_FLOAT, False, 3*4, ctypes.c_void_p(2*4))

        glDrawArrays(GL_TRIANGLES, 0, len(Explosion.VERTEX_BUFFER_RAW) / 3)

        glDisableVertexAttribArray(pos_location)
        glDisableVertexAttribArray(color_location)

        glBindBuffer(GL_ARRAY_BUFFER, 0)

        glPopMatrix()

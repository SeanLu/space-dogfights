from pygame import *

SCREEN_SIZE = (1280,768)

init()

PLAYER_RADIUS = 15
BULLET_RADIUS = 3
MISSILE_RADIUS = 5
POWERUP_RADIUS = 10

BACKGROUND_IMAGE = transform.scale(image.load("images/_0002_BG.png"), SCREEN_SIZE)
# need to be scaled by each instance
ASTEROID_IMAGE = image.load("images/_0003_asteroid.png")
"""
ROUNDSHIP_IMAGE = image.load("images/_0000_round-ship.png")
TRIANGLESHIP_IMAGE = image.load("images/_0001_triangle-ship.png")
"""
ROUNDSHIP_IMAGE = transform.scale(image.load("images/_0000_round-ship.png"), (PLAYER_RADIUS * 2, PLAYER_RADIUS * 2))
TRIANGLESHIP_IMAGE = transform.scale(image.load("images/_0001_triangle-ship.png"), (PLAYER_RADIUS * 2, PLAYER_RADIUS * 2))
CIRCLESHIP_IMAGE = transform.scale(image.load("images/circle-ship.png"), (PLAYER_RADIUS * 2, PLAYER_RADIUS * 2))
TRIDENTSHIP_IMAGE = transform.scale(image.load("images/trident-ship.png"), (PLAYER_RADIUS * 2, PLAYER_RADIUS * 2))

MISSILE_IMAGE = transform.scale(image.load("images/_0004_bullet.png"), (MISSILE_RADIUS * 2, MISSILE_RADIUS * 4))
#MISSILE_IMAGE = image.load("images/_0004_bullet.png")

POWERUP_SPEED_IMAGE = image.load("images/blueberry.png")
POWERUP_SHIELD_IMAGE = image.load("images/capsule.png")
POWERUP_SPLITSHOT_IMAGE = image.load("images/watermelon.png")
POWERUP_LAZER_IMAGE = image.load("images/orange.png")
POWERUP_HOMINGMISSILE_IMAGE = image.load("images/avocado.png")

screen=Surface(SCREEN_SIZE)
openglScreen=display.set_mode(SCREEN_SIZE, HWSURFACE|OPENGL|DOUBLEBUF)

BULLET_TYPE_NORMAL = 1

PLAYER_NAMES = {
    1: "Sean",
    2: "Peter",
    3: "Bailey",
    4: "Terrance"
}

GUN_TYPE = {
    "DEFAULT": 1,
    "LASER": 2,
    "HOMINGMISSILE": 3
}

FIRE_RATE = {
    1: 10, # one bullet per 10 frames for the default gun
    2: 0, # continuous beam for laser
    3: 15 # homing missile (not actually used, change in Player class if necessary)
}

BULLET_SPEED = {
    1: 12, # 12 pixels per frame for bullets from the default gun
    3: 6
}

BULLET_POWER = {
    1: 1, # 1 dmg for basic gun
    2: 0.25, # dmg for laser
    3: 1
}

BULLET_COLOR = {
    1: (100, 220, 240),
    3: (80, 200, 220)
}

POWERUP_TYPE = {
    -1: "None",
	0: "Speed",
	1: "Shield",
	2: "Splitshot",
	3: "Laser",
    4: "Homingmissile"
}

# Key is asteroid size, value is list of possible combinations of asteroids it can break up into
# This is a list of dictionaries with key asteroid size and value number generated
ASTEROID_BREAKUP = {
    1: [],
    2: [{1:4}, {1:3}],
    3: [{1:1, 2:2}, {1:7}],
    4: [{1:4, 2:1}, {1:2, 2:2}, {2: 3}, {1:1, 2:2, 3:1}, {1:9}],
    5: [{1:12}, {4:2}, {3:4}, {1:6, 2:3}, {1:2, 3:2, 4:1}, {1:1, 2:1, 3:1, 4:1}]
}

# Key is asteroid size, value is cumulative frequency
ASTEROID_DISTRIBUTION_FREQUECY = {
    1: 0.1,
    2: 0.25,
    3: 0.45,
    4: 0.7,
    5: 1.0
}

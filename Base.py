from config import *
from Sound import *
import Globals
from math import *

sounds = Sounds()

def clean_up(l):
    return filter(lambda x:x.exists, l)

def closest_approach(Ax0, Ay0, Ax1, Ay1, Bx0, By0, Bx1, By1):
    x0 = Bx0 - Ax0
    y0 = By0 - Ay0
    x1 = Bx1 - Ax1 - x0
    y1 = By1 - Ay1 - y0

    if x1 == y1 == 0:
        t = 0
    else:
        t = - float(x0*x1 + y0*y1) / (x1**2 + y1**2)

    if t < 0:
        t = 0
    elif t > 1:
        t = 1
        
    return ((x0 + t*x1)**2 + (y0 + t*y1)**2)**0.5

# Returns the new velocities, Avx', Avy', Bvx', Bvy'
def velocities_after_collision(Ax, Ay, Avx, Avy, Am, Bx, By, Bvx, Bvy, Bm):
    newAvx = Avx - 2.0*Bm/(Am+Bm) * ((Avx-Bvx)*(Ax-Bx)+(Avy-Bvy)*(Ay-By))/((Ax-Bx)**2 + (Ay-By)**2) * (Ax - Bx)
    newAvy = Avy - 2.0*Bm/(Am+Bm) * ((Avx-Bvx)*(Ax-Bx)+(Avy-Bvy)*(Ay-By))/((Ax-Bx)**2 + (Ay-By)**2) * (Ay - By)
    newBvx = Bvx - 2.0*Am/(Am+Bm) * ((Bvx-Avx)*(Bx-Ax)+(Bvy-Avy)*(By-Ay))/((Ax-Bx)**2 + (Ay-By)**2) * (Bx - Ax)
    newBvy = Bvy - 2.0*Am/(Am+Bm) * ((Bvx-Avx)*(Bx-Ax)+(Bvy-Avy)*(By-Ay))/((Ax-Bx)**2 + (Ay-By)**2) * (By - Ay)
    return newAvx, newAvy, newBvx, newBvy

def spree_type(spree):
    if spree >= 3:
        if spree > 10:
            spree = 10
        return ["on a Killing Spree", "Dominating", "on a Mega kill!", "Unstoppable!", "Wicked sick!", "on a Monster kill!", "Godlike!", "BEYOND GODLIKE!!"][spree-3]
    return ""

class Base(object):
    def __init__(self, x, y, vx, vy, r, hp=1):
        self.x = x
        self.y = y
        self.vx = vx
        self.vy = vy
        self.prevx = self.x
        self.prevy = self.y
        self.r = r
        self.player_number = 0
        self.power = 1
        self.exists = True
        self.health = 2
        self.invulnerability = 0

    def update(self, keys=None, bullets=None):
        pass

    def draw(self):
        pass

    def take_damage(self, source, **kwargs):
        from Player import *
        from Bullet import *

        banner_duration = 200
        banner_message = "afeaefawegaw"
        if source.player_number != self.player_number:
            if self.invulnerability == 0:
                dmg = source.power
                if isinstance(source, Player) and source.powerup == Powerup.TYPE_LAZER and source.action == Player.ACTION_SHOOTING:
                    dmg = source.powerup_level * BULLET_POWER[GUN_TYPE["LASER"]]
                self.health -= dmg
            if self.health <= 0 and (not isinstance(self, Player) or self.action != Player.ACTION_DYING):
                # increase kill count
                if self.player_number != 0 and source.player_number > 0:
                    if isinstance(source, Bullet):
                        player = source.player
                    elif isinstance(source, Player):
                        player = source
                    player.kill_count += 1
                    player.kill_spree += 1
                    sounds.play_spree(player.kill_spree)
                    if player.kill_spree >= 3:
                        Globals.banner_message = player.name()+" is " + spree_type(player.kill_spree)+"!!"
                        Globals.banner_duration = 200
                self.die(**kwargs)
            return True
        return False

    def cleanup(self):
        pass

    def load_images(self):
        pass
    
    def die(self):
        self.exists = False

    def get_angle_cc(self): #counter clockwise
        # angle will be used as counterclockwise
        vx = self.vx
        vy = self.vy
        if vx == 0:
            angle = 0
            if vy < 0:
                angle = 180
        else:
            angle = degrees(atan(fabs(vy)/fabs(vx)))
            # fix angle based on quadrant
            if vx > 0 and vy < 0:
                angle = 270 + angle
            elif vx > 0 and vy >= 0:
                angle = 270 - angle
            elif vx < 0 and vy >= 0:
                angle = 90 + angle
            elif vx < 0 and vy < 0: 
                angle = 90 - angle
        while angle < 0:
            angle += 360
        while angle > 360:
            angle -= 360
        return angle


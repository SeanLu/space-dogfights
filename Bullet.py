from Base import *
from pygame import *
from math import *

class Bullet(Base):
    def __init__(self, x, y, vx, vy, player, gun_type):
        # TODO: get r based on gun type
        speed = BULLET_SPEED[gun_type]        
        current_speed = sqrt(vx**2 + vy**2)
        vx = vx * speed / current_speed
        vy = vy * speed / current_speed

        super(Bullet,self).__init__(x, y, vx, vy, BULLET_RADIUS)
        self.player = player
        self.player_number = player.player_number
        self.gun_type = gun_type
        self.power = BULLET_POWER[gun_type]

    def draw(self):
        # TODO: 
        # make use of gun type
        #draw.circle(screen, BULLET_COLOR[self.gun_type], (int(self.x), int(self.y)), int(self.r))
        draw.line(screen, (BULLET_COLOR[self.gun_type]), (self.x - self.vx/2, self.y - self.vy/2), (self.x + self.vx/2, self.y + self.vy/2), self.r)


    def update(self, players=None):
        self.prevx = self.x
        self.prevy = self.y

        # TODO
        # potentially change vx and vy based on bullet type

        self.x += self.vx
        self.y += self.vy

        # reaches the edge of the screen
        if self.x < 0 or self.x > SCREEN_SIZE[0] or self.y < 0 or self.y > SCREEN_SIZE[1]:
            self.exists = False

class HomingMissile(Bullet):
    rotated_image_cached = {}

    def __init__(self, x, y, vx, vy, player, gun_type):
        super(HomingMissile,self).__init__(x,y,vx,vy,player,gun_type)
        self.player_target = None
        self.accel = 0.6 + 0.2 * player.powerup_level
        self.max_v = 16 + player.powerup_level
        self.r = MISSILE_RADIUS

    def update(self, players):
        if not self.player_target:
            # Acquire target
            smallest_angle = None
            for player in players:
                if player.player_number != self.player_number:
                    angle = acos((self.vx*(player.x-self.x) + self.vy*(player.y-self.y))/sqrt((self.vx**2+self.vy**2)*((player.x-self.x)**2+(player.y-self.y)**2)))
                    if not smallest_angle or angle < smallest_angle:
                        smallest_angle = angle
                        self.player_target = player

        accelx = self.player_target.x - self.x
        accely = self.player_target.y - self.y
        accel = sqrt(accelx**2+accely**2)
        accelx = accelx * self.accel / accel
        accely = accely * self.accel / accel

        self.vx += accelx
        self.vy += accely
        v = sqrt(self.vx**2+self.vy**2)
        if v > self.max_v:
            self.vx = self.vx * self.max_v / v
            self.vy = self.vy * self.max_v / v

        super(HomingMissile,self).update()


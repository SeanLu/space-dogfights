from OpenGL.GL import *

from Texture import Texture

class Framebuffer:
    def __init__(self, size):
        self.id = glGenFramebuffers(1)
        glBindFramebuffer(GL_FRAMEBUFFER, self.id)
        self.old_viewport = None
        self.texture = Texture(None, size)
        self.texture.bind()
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, self.texture.id, 0)
        glBindFramebuffer(GL_FRAMEBUFFER, 0)

    def bind(self):
        self.old_viewport = glGetIntegerv(GL_VIEWPORT)
        glBindFramebuffer(GL_FRAMEBUFFER, self.id)
        glViewport(0, 0, self.texture.size[0], self.texture.size[1])

    def unbind(self):
        glBindFramebuffer(GL_FRAMEBUFFER, 0)
        if (self.old_viewport != None):
            glViewport(self.old_viewport[0], self.old_viewport[1], self.old_viewport[2], self.old_viewport[3])

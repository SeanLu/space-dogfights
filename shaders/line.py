VERTEX_SHADER="""
#version 120

attribute vec2 aPos;
attribute vec4 aColor;

varying vec4 vColor;

void main() {
    vColor = aColor;
    gl_Position = gl_ModelViewProjectionMatrix * vec4(aPos, 0.0, 1.0);
}
"""

FRAGMENT_SHADER="""
#version 120

varying vec4 vColor;

void main() {
    gl_FragColor = vColor;
}
"""

VERTEX_SHADER="""
#version 120

attribute vec2 aPos;
attribute vec2 aTexCoord;

varying vec2 vTexCoord;

void main() {
    vTexCoord = aTexCoord;
    gl_Position = vec4(aPos, 0.0, 1.0);
}
"""

FRAGMENT_SHADER="""
#version 120

varying vec2 vTexCoord;

uniform sampler2D uTexture;

uniform vec2 uDirection;
uniform vec2 uScreenSize;

void main() {
    float weights[7];

    weights[0] = 924.0 / 2460.0;
    weights[1] = 742.0 / 2460.0;
    weights[2] = 495.0 / 2460.0;
    weights[3] = 220.0 / 2460.0;
    weights[4] =  66.0 / 2460.0;
    weights[5] =  12.0 / 2460.0;
    weights[6] =   1.0 / 2460.0;

    vec4 total = vec4(0);

    for (int i = -6; i <= 6; i++) {
        total += 0.7 * weights[int(abs(i))] * texture2D(uTexture, vTexCoord + (float(i) * 1.0 * uDirection) / uScreenSize);
    }
    gl_FragColor = vec4(total.xyz, 1.0);
}
"""

VERTEX_SHADER="""
#version 120

attribute vec2 aPos;
attribute float aColor;

varying float vColor;

void main() {
    vColor = aColor;
    gl_Position = gl_ModelViewProjectionMatrix * vec4(aPos, 0.0, 1.0);
}
"""

FRAGMENT_SHADER="""
#version 120

varying float vColor;

uniform float uAlpha;

void main() {
    gl_FragColor = vec4(vec3(1), vColor * uAlpha);
}
"""

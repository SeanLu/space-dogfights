from Base import *
from pygame import *
from math import *
from random import *

class Powerup(Base):
	images=[]
	TYPE_SPEED = 0
	TYPE_SHIELD = 1
	TYPE_SPLITSHOT = 2
	TYPE_LAZER = 3
	TYPE_HOMINGMISSILE = 4

	def __init__(self, powerup_type):
		super(Powerup,self).__init__(randint(50, SCREEN_SIZE[0] - 50), randint(50, SCREEN_SIZE[1] - 50), 0, 0, POWERUP_RADIUS)
		self.type = powerup_type
		if self.type == Powerup.TYPE_SPEED:
			self.image = transform.scale(POWERUP_SPEED_IMAGE, (self.r * 2, self.r * 2))
		elif self.type == Powerup.TYPE_SHIELD:
			self.image = transform.scale(POWERUP_SHIELD_IMAGE, (self.r * 2, self.r * 2))
		elif self.type == Powerup.TYPE_SPLITSHOT:
			self.image = transform.scale(POWERUP_SPLITSHOT_IMAGE, (self.r * 2, self.r * 2))
		elif self.type == Powerup.TYPE_LAZER:
			self.image = transform.scale(POWERUP_LAZER_IMAGE, (self.r * 2, self.r * 2))
		elif self.type == Powerup.TYPE_HOMINGMISSILE:
			self.image = transform.scale(POWERUP_HOMINGMISSILE_IMAGE, (self.r * 2, self.r * 2))
	
	def draw(self):
		screen.blit(self.image, (int(self.x - self.r), int(self.y - self.r)))
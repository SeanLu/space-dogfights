#!/usr/bin/python

import pygame
from pygame import *
from random import *
import datetime
import cPickle
import os,sys
from math import *

import Globals

from Base import *
from Player import *
from Asteroid import *
from Sound import *

pygame.font.init()
pygame.joystick.init()
joystick1 = None
joystick2 = None
#joystick_names = []
#for i in range(0, pygame.joystick.get_count()):
#    joystick_names.append(pygame.joystick.Joystick(i).get_name())
#print pygame.joystick.get_count()
#print joystick_names
if pygame.joystick.get_count() > 0:
    joystick1 = pygame.joystick.Joystick(0)
    joystick1.init()

if pygame.joystick.get_count() > 1:
    joystick2 = pygame.joystick.Joystick(1)
    joystick2.init()

Globals.init()
default_font=pygame.font.Font(None,14)
default_font_big=pygame.font.Font(None,50)
textDefaultColor = (150, 100, 100)
def display_text(message, pos, font=default_font, colour=textDefaultColor, center= False): #puts in a string and display it out on the position
    text = font.render(message,1,colour)
    if center:
        textpos = text.get_rect().move(pos[0] - text.get_rect().width/2,pos[1])
    else:
        textpos = text.get_rect().move(pos[0],pos[1])
    screen.blit(text, textpos)

from Shader import Shader
from Texture import Texture
from TextureDrawer import TextureDrawer
from Framebuffer import Framebuffer
from LineDrawer import LineDrawer
from Explosion import Explosion

from OpenGL.GL import *
from OpenGL.GLU import *

import shaders.blit as blit
import shaders.blur as blur
import shaders.line as line
import shaders.explosion as explosion

systime=datetime.datetime.now()

players = []
players.append(Player(1))
players.append(Player(2))
players.append(Player(3))
players.append(Player(4))

powerups = []
powerups_spawn_cd = 0

asteroids = []
asteroids_spawn_cd = 0

explosions = []

blitShader = Shader(blit.VERTEX_SHADER, blit.FRAGMENT_SHADER)
blurShader = Shader(blur.VERTEX_SHADER, blur.FRAGMENT_SHADER)
lineShader = Shader(line.VERTEX_SHADER, line.FRAGMENT_SHADER)
explosionShader = Shader(explosion.VERTEX_SHADER, explosion.FRAGMENT_SHADER)
texture_drawer = TextureDrawer()

glMatrixMode(GL_PROJECTION)
glLoadIdentity()
glOrtho(0, SCREEN_SIZE[0], SCREEN_SIZE[1], 0, -1, 1)
glMatrixMode(GL_MODELVIEW)
glDisable(GL_DEPTH_TEST)

bullets = []

glowFBO = Framebuffer(SCREEN_SIZE)

tempFBO1 = Framebuffer(SCREEN_SIZE)
tempFBO2 = Framebuffer(SCREEN_SIZE)

def draw_lazer_borders():
    SIDE_LAZER_SIZE = 2
    SIDE_LAZER_INSET = 5

    draw.rect(screen, (255, 0, 0), (SIDE_LAZER_INSET, SIDE_LAZER_INSET, SCREEN_SIZE[0] - 2*SIDE_LAZER_INSET, SCREEN_SIZE[1] - 2*SIDE_LAZER_INSET), SIDE_LAZER_SIZE)

def draw_player_trails():
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    lineShader.bind()
    batch = LineDrawer()
    batch.begin()
    for player in players:
        player.drawPlayerTrail(batch)
    batch.end(lineShader)
    glDisable(GL_BLEND)

def drawExplosions():
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    explosionShader.bind()
    for explosion in explosions:
        explosion.draw(explosionShader)
    glDisable(GL_BLEND)

myClock=time.Clock()
running=True
while running:
    for evnt in event.get():
        if evnt.type==QUIT:
            running=False

    keys=key.get_pressed()
    keys=list(keys)
    mx,my=mouse.get_pos()
    mbl,mbm,mbr=mouse.get_pressed()

    if joystick1 is not None:
        if joystick1.get_button(14):
            keys[K_4] = True
        if joystick1.get_button(4):
            keys[K_0] = True
        if joystick1.get_button(7):
            keys[K_1] = True
        if joystick1.get_button(6):
            keys[K_2] = True
        if joystick1.get_button(5):
            keys[K_3] = True

    if joystick2 is not None:
        if joystick2.get_button(14):
            keys[K_g] = True
        if joystick2.get_button(4):
            keys[K_7] = True
        if joystick2.get_button(7):
            keys[K_y] = True
        if joystick2.get_button(6):
            keys[K_u] = True
        if joystick2.get_button(5):
            keys[K_i] = True

    ##mouse translation#
    #mousex = mx - players[3].x
    #mousey = my - players[3].y
    #mouse_threshold = 50
    #if mousex**2 + mousey**2 > mouse_threshold **2:
    #    if mousey < mouse_threshold:
    #        keys[K_0] = True
    #    if mousex < mouse_threshold:
    #        keys[K_1] = True
    #    if mousey > mouse_threshold:
    #        keys[K_2] = True
    #    if mousex > mouse_threshold:
    #        keys[K_3] = True
    #if mbl:
    #    keys[K_4] = True
    #if mbr:
    #    keys[K_5] = True

    ###bg music##
    if mixer.get_busy()==0:
        sounds.play_background()

    ##spawning###

    powerups_spawn_cd -= 1
    if powerups_spawn_cd <= 0:
        powerups.append(Powerup(randint(0,4)))
        powerups_spawn_cd = 10*1.5**len(powerups)

    effective_total_size = int(sum(map(lambda x:ASTEROID_DISTRIBUTION_FREQUECY[x.size], asteroids)))
    if randint(1,(effective_total_size**2/2+1)) == 1:
        r = random()
        for i in range(1,6):
            if r < ASTEROID_DISTRIBUTION_FREQUECY[i]:
                size = i
                break
        asteroids.append(Asteroid(size))

    for asteroid in asteroids:
        asteroid.update()

    for player in players:
        player.update(keys, bullets, explosions=explosions)

    # update bullets
    for bullet in bullets:
        bullet.update(players=players)

    for explosion in explosions:
        explosion.update()

    explosions = filter(lambda x: x.life > 0, explosions)

    #plaver vs player collision
    for player1 in players:
        for player2 in players:
            if player1.player_number != player2.player_number:
                if closest_approach(player1.prevx, player1.prevy, player1.x, player1.y, player2.prevx, player2.prevy, player2.x, player2.y) < player1.r + player2.r and player1.action!=Player.ACTION_DYING and player2.action!=Player.ACTION_DYING:
                    player1.take_damage(player2)
                    player2.take_damage(player1)

    #player vs asteroid collision
    for player in players:
        for asteroid in asteroids:
            if closest_approach(asteroid.prevx, asteroid.prevy, asteroid.x, asteroid.y, player.prevx, player.prevy, player.x, player.y) < player.r + asteroid.r and player.action!=Player.ACTION_DYING:
                if player.take_damage(asteroid):
                    asteroid.die(asteroid_list=asteroids)

    #player vs bullet collision
    for player in players:
        for bullet in bullets:
            if closest_approach(bullet.prevx, bullet.prevy, bullet.x, bullet.y, player.prevx, player.prevy, player.x, player.y) < player.r + bullet.r and player.action!=Player.ACTION_DYING:
                if player.take_damage(bullet):
                    bullet.die()
        # check for lasers
        if player.action == Player.ACTION_SHOOTING and player.powerup == Powerup.TYPE_LAZER:
            speed = sqrt(player.vx**2 + player.vy**2)
            vx = player.vx * sqrt(2) / speed
            vy = player.vy * sqrt(2) / speed
            xprev = player.x
            yprev = player.y
            x = xprev + vx * SCREEN_SIZE[0]
            y = yprev + vy * SCREEN_SIZE[1]
            for p in players:
                if closest_approach(xprev, yprev, x, y, p.prevx, p.prevy, p.x, p.y) < p.r + player.powerup_level and player.action != Player.ACTION_DYING:
                    p.take_damage(player)
            for asteroid in asteroids:
                if closest_approach(xprev, yprev, x, y, asteroid.prevx, asteroid.prevy, asteroid.x, asteroid.y) < asteroid.r + player.powerup_level:
                    asteroid.take_damage(player, asteroid_list=asteroids)
            for powerup in powerups:
                if closest_approach(xprev, yprev, x, y, powerup.prevx, powerup.prevy, powerup.x, powerup.y) < powerup.r + player.powerup_level:
                    powerup.take_damage(player)

    #asteroid vs bullet collision
    for asteroid in asteroids:
        for bullet in bullets:
            if closest_approach(bullet.prevx, bullet.prevy, bullet.x, bullet.y, asteroid.prevx, asteroid.prevy, asteroid.x, asteroid.y) < asteroid.r + bullet.r:
                if asteroid.take_damage(bullet, asteroid_list=asteroids):
                    bullet.die()

    #powerup vs bullet collision
    for powerup in powerups:
        for bullet in bullets:
            if closest_approach(bullet.prevx, bullet.prevy, bullet.x, bullet.y, powerup.prevx, powerup.prevy, powerup.x, powerup.y) < powerup.r + bullet.r:
                if powerup.take_damage(bullet):
                    bullet.die()
    #player taking powerups
    for player in players:
        for powerup in powerups:
            if closest_approach(powerup.prevx, powerup.prevy, powerup.x, powerup.y, player.prevx, player.prevy, player.x, player.y) < player.r + powerup.r and player.action!=Player.ACTION_DYING:
                if player.powerup != powerup.type:
                    player.powerup_level -= 1
                else:
                    player.powerup_level += 1
                if player.powerup_level <= 0:
                    player.powerup_level = 1
                player.powerup = powerup.type
                powerup.die()
                player.update_powerup()

    for asteroid1 in clean_up(asteroids):
        for asteroid2 in asteroids[asteroids.index(asteroid1)+1:]:
            if closest_approach(asteroid1.prevx, asteroid1.prevy, asteroid1.x, asteroid1.y, asteroid2.prevx, asteroid2.prevy, asteroid2.x, asteroid2.y) < asteroid1.r + asteroid2.r:
                # Make sure at least one of the asteroids is going in the direction of the other to avoid edge cases where there has already been a collision but in the next frame it still detects a collision
                if asteroid1.vx*(asteroid2.x-asteroid1.x)+asteroid1.vy*(asteroid2.y-asteroid1.y) > 0 or asteroid2.vx*(asteroid1.x-asteroid2.x)+asteroid2.vy*(asteroid1.y-asteroid2.y) > 0:
                    asteroid1.vx, asteroid1.vy, asteroid2.vx, asteroid2.vy = velocities_after_collision(asteroid1.x, asteroid1.y, asteroid1.vx, asteroid1.vy, (asteroid1.r)**2, asteroid2.x, asteroid2.y, asteroid2.vx, asteroid2.vy, (asteroid2.r)**2)
                    asteroid1.x = asteroid1.prevx
                    asteroid1.y = asteroid1.prevy
                    asteroid2.x = asteroid2.prevx
                    asteroid2.y = asteroid2.prevy

    # draw all elements that need to have a bloom

    screen.fill((0, 0, 0))

    # update bullets
    for bullet in bullets:
        bullet.draw()

    for player in players:
        player.drawGlow()

    for explosion in explosions:
        explosion.drawGlow()

    draw_lazer_borders()

    bytes = image.tostring(screen,"RGBA",1)
    glowTexture = Texture(bytes, SCREEN_SIZE)
    tempFBO1.bind()
    blitShader.bind()
    texture_drawer.draw(glowTexture, blitShader)
    drawExplosions()
    draw_player_trails()
    tempFBO1.unbind()

    # draw final scene

    screen.blit(BACKGROUND_IMAGE, (0, 0))

    for powerup in powerups:
        powerup.draw()
    
    for asteroid in asteroids:
        asteroid.draw()

    for player in players:
        player.draw()

    # update bullets
    for bullet in bullets:
        bullet.draw()

    for explosion in explosions:
        explosion.drawGlow()

    draw_lazer_borders()

    # draw the stats
    for player in players:
        x = player.scoreboard_pos[0]
        y = player.scoreboard_pos[1]
        display_text(player.name() + ":", (x, y))
        y += 15
        display_text("Kills: " + str(player.kill_count), (x, y))
        y += 15
        display_text("Deaths: " + str(player.death_count), (x, y))
        y += 15
        display_text("Killing spree: " + str(player.kill_spree), (x, y))
        y += 15
        powerup_type = POWERUP_TYPE[player.powerup]
        powerup_level = player.powerup_level
        powerup_msg = "Powerup: " + powerup_type
        display_text(powerup_msg, (x, y))
        if powerup_level > 0:
            y += 15
            powerup_level_msg = "Powerup level: " + str(powerup_level)
            display_text(powerup_level_msg, (x, y))
    

    #draw banner
    Globals.banner_duration -= 1
    if Globals.banner_duration > 0:
        display_text(Globals.banner_message, (SCREEN_SIZE[0]/2, 30), font = default_font_big, center=True)

    #### clean up ####
    asteroids = clean_up(asteroids)
    powerups = clean_up(powerups)
    bullets = clean_up(bullets)

    bytes = image.tostring(screen,"RGBA",1)
    texture = Texture(bytes, SCREEN_SIZE)

    for i in range(5):
        tempFBO2.bind()
        blurShader.bind()
        blurShader.setUniform2f("uScreenSize", SCREEN_SIZE[0], SCREEN_SIZE[1])
        blurShader.setUniform2f("uDirection", 1, 0)
        texture_drawer.draw(tempFBO1.texture, blurShader)
        tempFBO2.unbind()

        tempFBO1.bind()
        blurShader.bind()
        blurShader.setUniform2f("uScreenSize", SCREEN_SIZE[0], SCREEN_SIZE[1])
        blurShader.setUniform2f("uDirection", 0, 1)
        texture_drawer.draw(tempFBO2.texture, blurShader)
        tempFBO1.unbind()

    glClearColor(0.0, 1.0, 0.0, 1.0)
    glClear(GL_COLOR_BUFFER_BIT)
    blitShader.bind()
    texture_drawer.draw(texture, blitShader)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE)
    texture_drawer.draw(tempFBO1.texture, blitShader)
    glDisable(GL_BLEND)

    glowTexture.delete()
    texture.delete()

    display.flip()
    myClock.tick(50)

    err = glGetError()
    if ( err != GL_NO_ERROR ):
        print 'GLERROR: ', gluErrorString( err )
        sys.exit()

quit()


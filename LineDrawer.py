from OpenGL.GL import *

import numpy

from TextureDrawer import TextureDrawer

class LineDrawer:
    POS_ATTRIB = TextureDrawer.POS_ATTRIB
    COLOR_ATTRIB = "aColor"


    def __init__(self):
        self.vertex_data=[]

    def begin(self):
        self.vertex_data=[]

    def line(self, start, end, start_color, end_color=None):
        if end_color is None:
            end_color = start_color
        self.vertex_data.extend(start)
        self.vertex_data.extend(start_color)
        self.vertex_data.extend(end)
        self.vertex_data.extend(end_color)

    def end(self, shader):
        vertex_buffer = glGenBuffers(1)

        vertices = numpy.array(self.vertex_data, dtype=numpy.float32)

        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer)

        # Send the data over to the buffer
        glBufferData(GL_ARRAY_BUFFER, len(self.vertex_data)*4, vertices, GL_STATIC_DRAW)

        glBindBuffer(GL_ARRAY_BUFFER, 0)

        pos_location = shader.getAttribLocation(LineDrawer.POS_ATTRIB)
        color_location = shader.getAttribLocation(LineDrawer.COLOR_ATTRIB)

        glEnableVertexAttribArray(pos_location)
        glEnableVertexAttribArray(color_location)

        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer)

        # Describe the position data layout in the buffer
        glVertexAttribPointer(pos_location, 2, GL_FLOAT, False, 6*4, ctypes.c_void_p(0))
        glVertexAttribPointer(color_location, 4, GL_FLOAT, False, 6*4, ctypes.c_void_p(2*4))

        glDrawArrays(GL_LINES, 0, len(self.vertex_data) / 6)

        glDisableVertexAttribArray(pos_location)
        glDisableVertexAttribArray(color_location)

        glBindBuffer(GL_ARRAY_BUFFER, 0)

        glDeleteBuffers(1, [vertex_buffer])
